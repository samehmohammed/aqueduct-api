import 'package:aqueduct_api/aqueduct_api.dart';
import 'package:aqueduct_api/model/heroes-model.dart';

class HeroesController extends ResourceController{

  HeroesController(this.context);

  final ManagedContext context;

// final _heroes = [
//     {'id': 11, 'name': 'Mr. Nice'},
//     {'id': 12, 'name': 'Narco'},
//     {'id': 13, 'name': 'Bombasto'},
//     {'id': 14, 'name': 'Celeritas'},
//     {'id': 15, 'name': 'Magneta'},    
//   ];
 

  @Operation.get()
  Future<Response> getAllHeroes({@Bind.query('name') String name}) async{

    final heroQuery = Query<Hero>(context)
    ..sortBy((u) => u.id, QuerySortOrder.ascending);

    if(name != null) {
       heroQuery.where((h) => h.name).contains(name , caseSensitive: false);
    }
    final heroes =await heroQuery.fetch();
    return Response.ok(heroes);
  }

  @Operation.get('id')
  Future<Response> getSingleHeroe(@Bind.path('id') int id) async{
    final heroQuery = Query<Hero>(context)
    ..where((h) => h.id).equalTo(id);
    final hero = await heroQuery.fetchOne();
     if (hero == null) {
    return Response.notFound();
    }
    return Response.ok(hero);

  }


@Operation.post()
Future<Response> createHero() async{

  final Map<String, dynamic> body = await request.body.decode();

  final query = Query<Hero>(context)
  ..values.name = body['name'] as String;

  final insertHero = await query.insert();
  return Response.ok(insertHero);
}

@Operation.put('id')
Future<Response> editHero(@Bind.path('id') int id,) async{
  final Map<String, dynamic> body = await request.body.decode();
  final query = Query<Hero>(context)..where((h) =>h.id ).equalTo(id)
  ..values.name = body['name'] as String;
  return Response.ok(await query.updateOne());
}

@Operation.delete('id')
Future<Response> deleteHero(@Bind.path('id') int id) async{
  final query = Query<Hero>(context)..where((h) =>h.id ).equalTo(id);
  final updateHero = query.delete();
  return Response.ok('delete ...');
}


}